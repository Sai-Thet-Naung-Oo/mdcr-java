package chapter_9;

import java.util.Scanner;

public class Unit_One_Array {
	public static void main(String[] args) {
		int[] a = new int[5];
		int i, j;
		Scanner scanner = new Scanner(System.in);

		for (i = 0; i < a.length; i++) {
			System.out.print("Enter Number: ");
			a[i] = scanner.nextInt();
			for (j = 0; j < i; j++) {
				while (a[i] == a[j]) {
					{
						System.out.println("Duplicate ");
						System.out.println("Enter next number ");
						a[i] = scanner.nextInt();
						j = 0;
					}
				}
			}
		}
		for (i = 0; i < a.length; i++) {
			System.out.print(a[i]);
		}
	}
}
