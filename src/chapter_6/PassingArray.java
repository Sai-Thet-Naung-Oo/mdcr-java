package chapter_6;

public class PassingArray {

	public static void main(String[] args) {
		int[] a = { 6, 1, 3 };
		display(a);

		change(a);

		display(a);
	}

	public static void display(int[] b) {
		for (int i = 0; i < b.length; i++) {
			System.out.print(b[i] + "\t");
		}
		System.out.println();
	}

	public static void change(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] *= 3;
		}
	}
}
