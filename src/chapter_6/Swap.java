package chapter_6;

public class Swap {

	public static void main(String[] args) {
		int a = 13, b = 99;

		display(a, b); // Actual Args

		swapData(a, b);

		display(a, b); // Actual Args
	}

	public static void display(int a, int b) {
		System.out.print("a = " + a + ", ");
		System.out.println("b = " + b);
	}

	public static void swapData(int x, int y) { // formal Parameters
		int temp = x;
		x = y;
		y = temp;

		System.out.println("x = " + x + ", y = " + y);
	}

}
