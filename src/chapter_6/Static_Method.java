package chapter_6;

public class Static_Method {

	public static void main(String[] args) {
		max(); // Method call

		int ans = min();
		System.out.println("Min = " + ans);
	}

	public static void max() { // Method Defination
		int a = 5, b = 2, c = 7;

		if (a > b && a > c) {
			System.out.println("Max number is " + a);
		} else if (b > a && b > c) {
			System.out.println("Max number is " + b);
		} else {
			System.out.println("Max number is " + c);
		}
	}

	public static int min() { // Method Defination
		int a = 5, b = 2, c = 7;

		if (a < b && a < c) {
			return a;
		} else if (b < a && b < c) {
			return b;
		} else {
			return a;
		}
	}

}
