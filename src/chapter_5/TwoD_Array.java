package chapter_5;

public class TwoD_Array {

	public static void main(String[] args) {
		int [][] twoDArray = {{1, 2, 3}, {4, 5, 6}};
		String [] names = {"Susan", "John"};
		int total = 0;
		
		System.out.println("Names\tModule_1\tModule_2\tModule_3\tTotal");
		
		for(int r = 0; r < twoDArray.length; r++) {

			System.out.print(names[r] + "\t");

			for(int c = 0; c < twoDArray[r].length; c ++) {
				System.out.print(twoDArray[r][c] + "\t\t");
				total += twoDArray[r][c];
			}
			
			System.out.println(total);
			total = 0;
		}
	}
}
