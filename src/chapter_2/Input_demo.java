package chapter_2;

import java.util.Scanner;

public class Input_demo {

	public static void main(String[] args) {
		
		int number;
				
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter Number: ");
		number = Integer.parseInt(scanner.nextLine());
		
		System.out.println("Your number is " + number);
		
	}
}
