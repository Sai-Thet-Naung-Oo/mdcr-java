package chapter_2;

public class TypeCasting_demo {

	public static void main(String[] args) {
		byte b;
		int i = 34;
		
		b = (byte) i;
		
		i = b;
		
		System.out.println(b);
		System.out.println(i);
		
		float f = 342.34f;
		int _i;
		
		_i = (int) f;
		
		System.out.println(_i);
	}
}
