package chapter_4;

public class Ch_4_ass_3 {

	public static void main(String[] args) {
		int sum = 0, i;

		for (i = 2; i < 100; i += 3) {
			if ((i % 5) == 0) {
				sum += i;
			}
		}

		System.out.println("Sum = " + sum);
	}
}
