package chapter_4;

import java.util.Scanner;

public class WhileDemo {

	public static void main(String[] args) {
		/*
		 * Write a program to find the sum of any numbers 
		 * input from the keyboard until input number is zero
		 * 
		 * */
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter first Number: "); 
		int num = scanner.nextInt();
		int sum = 0;
		
		while(num != 0) {
			sum += num;
			
			System.out.print("Enter another Number: "); 
			num = scanner.nextInt();
		}
		
		System.out.println("Sum = " +  sum);
		
//		int sec = 366527;
		
		
		
		
		
		
		
		
	}
}
