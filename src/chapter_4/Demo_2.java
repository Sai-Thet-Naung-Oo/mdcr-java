package chapter_4;

import java.util.Scanner;

public class Demo_2 {

	public static void main(String[] args) {
		int num, i, sum = 0, evenCount = 0;
		int oddCount = 0, max, min, avg = 0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter First Number: ");
		num = scanner.nextInt();
		max = min = sum = num;

		for (i = 1; i <= 4; i++) {
			System.out.print("Enter Number: ");
			num = scanner.nextInt();

			if (num > max) {
				max = num;
			}
			if (num < min) {
				min = num;
			}

			if (num % 2 == 0) {
				evenCount++;
			} else {
				oddCount++;
			}

			sum = sum + num;
		}

		avg = sum / 5;
		
		System.out.println("Sum = " + sum);
		System.out.println("Max = " + max);
		System.out.println("Min = " + min);
		System.out.println("Avg = " + avg);
		System.out.println("Total = " + sum);

	}
}
