package chapter_4;

public class Demo_3 {

	public static void main(String[] args) {
		int i, j, k;
		for (i = 1; i <= 5; i++) {
			for(j = 4; j >= i; j--) {
				System.out.print("\t");
			}
			for(k = 1; k <= i; k++) {
				System.out.print("*\t");
			}
			System.out.println();
		}
	}
}
