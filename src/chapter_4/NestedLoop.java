package chapter_4;

public class NestedLoop {

	public static void main(String[] args) {
//		
//		1   	1   2  3
//		2 		1   2  3
//		3		1   2  3
//		4		1   2  3
//		5  		1   2  3
		
		int i, j;
		
		for(i = 1 ; i <= 5; i++) {
			System.out.print(i + "\t");
			for(j = 1; j <= 3; j++) {
				System.out.print(j + "\t");
			}
			System.out.println();
		}
		
		
		
		
		
	}
}
