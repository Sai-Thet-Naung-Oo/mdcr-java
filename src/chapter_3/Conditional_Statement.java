package chapter_3;

public class Conditional_Statement {

	public static void main(String[] args) {
		int a = 1, b = 2, c;
		
		if(a > b) {
			c = a;
		}else {
			c = b;
		}
		
		c = (a > b)? a : b;
		
		System.out.println("c = " + c);		
		
		System.out.println("c = " + ((a > b)? a : b));
		
		
				
				
				
				
				
				
	}
}
